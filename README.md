# Project_2_Auraforce
    This project is designed to consume a REST API and use it in Aura Components within a lightning community. It consumes a Formula 1 API and provides relevant information from it.

    Features:
    - Map to display current season.
    - Discussion topics for users.
    - Dashboard to view statistics on each driver and race.
    - Driver view components that link to record detail page, as well as Driver Lists that you can add to.
    - Race Lists that can be created or deleted via an aura component.
    - Screen flow to allow for populating Race Lists, as well as creating them.

    Setup:
    1. Create new Org
    2. Enable community and create a new community called Formula 1 stats (domain does not matter). The template is Customer Service.
    3. Activate the community.
    4. Retrieve data from GitLab Repo.
    5. Change the site metadata so that the admin and owner are your username
    6. Deploy data to Org.

    Contributor:
    Wesley Williams, V "Quinn"
