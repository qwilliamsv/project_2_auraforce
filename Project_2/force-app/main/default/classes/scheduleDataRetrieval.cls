global class scheduleDataRetrieval implements Schedulable{
    global void execute(SchedulableContext sc){//Run the schedulable so that it keeps up to date with the current information from the API (Nightly is good)
        CalloutsClass.createCurrentDrivers();
        CalloutsClass.createCurrentRaces();
        CalloutsClass.createCurrentConstructors();
        CalloutsClass.createRaceResults();
    }
}