public class RaceMapApexController {
    @AuraEnabled
    public static List<Map<String,String>> getCourseLocations(){
        List<Map<String,Object>> raceList = CalloutsClass.getCurrentSchedule();//Fetches current season schedule and parses through to get location and information
        List<Map<String,String>> finalRaceList = new List<Map<String,String>>();
        for(Map<String,Object> raceListHold : raceList){
            Map<String,String> holder = new Map<String,String>();//Creates a map for the current race so that the info is easily accessible
            String raceName = JSON.serialize(raceListHold.get('raceName'));
            holder.put('raceName',raceName);
            String raceDate = JSON.serialize(raceListHold.get('date'));
            holder.put('date', raceDate);
            String raceTime = JSON.serialize(raceListHold.get('time'));
            holder.put('time', raceTime);
            String raceUrl = JSON.serialize(raceListHold.get('url'));
            holder.put('url', raceUrl);
            String circuitInfo = JSON.serialize(raceListHold.get('Circuit'));
            Map<String,Object> circuit = (Map<String,Object>) JSON.deserializeUntyped(CircuitInfo);
            String circuitName = JSON.serialize(circuit.get('circuitName'));
            holder.put('circuitName',circuitName);
            String circuitUrl = JSON.serialize(circuit.get('url'));
            holder.put('circuitUrl',circuitUrl);
            String locationHold = JSON.serialize(circuit.get('Location'));
            Map<String,Object> location = (Map<String,Object>) JSON.deserializeUntyped(locationHold);
            String lat = JSON.serialize(location.get('lat'));
            holder.put('lat',lat);
            String longi = JSON.serialize(location.get('long'));
            holder.put('long',longi);
            finalRaceList.add(holder);//Adds current map to list for easy access later
        }
        return finalRaceList;
    }
}