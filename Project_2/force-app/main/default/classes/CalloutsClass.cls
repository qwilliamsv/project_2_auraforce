public class CalloutsClass {
    public static Integer year = System.today().year();//Fetches current year to use to make callouts to the API
    public static List<Map<String,Object>> getCurrentSchedule(){
        Http http = new Http();
        HttpRequest request = new HttpRequest();
        request.setEndpoint('http://ergast.com/api/f1/current.json');//Gets current season race schedule
        request.setMethod('GET');
        HttpResponse response = http.send(request);
        if(response.getStatusCode() == 200){
            //Upon success, it has to parse through to get to the data we actually want, based on how the API returns it
            Map<String,Object> respMap = (Map<String,Object>) JSON.deserializeUntyped(response.getBody());
            String MRData = JSON.serialize(respMap.get('MRData'));
            Map<String,Object> MRDataMap = (Map<String,Object>) JSON.deserializeUntyped(MRData);
            String RaceTable = JSON.serialize(MRDataMap.get('RaceTable'));
            Map<String,Object> RaceTableMap = (Map<String,Object>) JSON.deserializeUntyped(RaceTable);
            String Races = JSON.serialize(RaceTableMap.get('Races'));
            List<Map<String,Object>> RaceList = new List<Map<String,Object>>();
            List<Object> RacesObjectList = (List<Object>) JSON.deserializeUntyped(Races);
            for(Object itemObj : RacesObjectList){
                Map<String,Object> hold = (Map<String,Object>) itemObj;
                RaceList.add(hold);
            }
            return RaceList;//The map holds the information we want, which can then be used later to fetch information about the individual races, parse through to get circuit info and coordinates
        }
        return null;
    }
    public static List<Map<String,Object>> getCurrentDrivers(){
        Http http = new Http();
        HttpRequest request = new HttpRequest();
        request.setEndpoint('http://ergast.com/api/f1/'+year+'/drivers.JSON');//Gets this years drivers
        request.setMethod('GET');
        HttpResponse response = http.send(request);
        if(response.getStatusCode() == 200){
            //Again, the API returns in several layers of Maps, so here parses through those layers
            Map<String,Object> respMap = (Map<String,Object>) JSON.deserializeUntyped(response.getBody());
            String MRData = JSON.serialize(respMap.get('MRData'));
            Map<String,Object> MRDataMap = (Map<String,Object>) JSON.deserializeUntyped(MRData);
            String DriverTable = JSON.serialize(MRDataMap.get('DriverTable'));
            Map<String,Object> DriverTableMap = (Map<String,Object>) JSON.deserializeUntyped(DriverTable);
            String Drivers = JSON.serialize(DriverTableMap.get('Drivers'));
            List<Object> RacersObjects = (List<Object>) JSON.deserializeUntyped(Drivers);
            List<Map<String,Object>> Racers = new List<Map<String,Object>>();
            for(Object racerObj : RacersObjects){
                Map<String,Object> hold = (Map<String,Object>) racerObj;
                Racers.add(hold);
            }
            return Racers;//Returns current racer information
        }
        return null;
    }
    @future //Enables for the schedulable class to utilize this
    public static void createCurrentDrivers(){
        List<Map<String,Object>> Racers = CalloutsClass.getCurrentDrivers();//Fetches the information
        if(Racers!=null){//Checks success
            List<Driver__c> addDrivers = new List<Driver__c>();
            List<Driver__c> alreadyAddedDrivers = [SELECT Id, Name FROM Driver__c];//Simple check to ensure no duplicates
            List<String> alreadyAddedNames = new List<String>();
            for(Driver__c hold : alreadyAddedDrivers){
                alreadyAddedNames.add(hold.Name);
            }
            for(Map<String,Object> Racer : Racers){//Creates new racers for all in the map if they do not already exist
                String currentName = JSON.serialize(Racer.get('givenName')).replaceAll('"','') + ' ' + JSON.serialize(Racer.get('familyName')).replaceAll('"','');
                if(!alreadyAddedNames.contains(currentName)){
                	Driver__c pilot = new Driver__c(Name=currentName,
                                                   Date_of_Birth__c=JSON.serialize(Racer.get('dateOfBirth')).replaceAll('"',''),
                                                   Driver_URL__c=JSON.serialize(Racer.get('url')).replaceAll('"',''),
                                                   Nationality__c=JSON.serialize(Racer.get('nationality')).replaceAll('"',''),
                                                   Permanent_Number__c=JSON.serialize(Racer.get('permanentNumber')).replaceAll('"',''));
                    addDrivers.add(pilot);
                }
            }
            insert addDrivers;
        }
    }
    @future
    public static void createCurrentRaces(){
        List<Map<String,Object>> Races = CalloutsClass.getCurrentSchedule();//Fetches current season
        if(Races!=null){//Checks success
            List<Race__c> addRaces = new List<Race__c>();
            List<Race__c> alreadyAddedRaces = [SELECT Name, Season__c FROM Race__c];//Simple checks to ensure the races don't already exist (in name and season)
            List<String> alreadyAddedRaceName = new List<String>();
            List<String> alreadyAddedRaceSeason = new List<String>();
            for(Race__c hold : alreadyAddedRaces){
                alreadyAddedRaceName.add(hold.Name);
                alreadyAddedRaceSeason.add(hold.Season__c);
            }
            for(Map<String,Object> Race : Races){//Creates race object after parsing information as needed, performs simple check to ensure the race doesn't already exist for this season
                String raceName = JSON.serialize(Race.get('raceName')).replaceAll('"','');
                String season = JSON.serialize(Race.get('season')).replaceAll('"','');
                if(!alreadyAddedRaceName.contains(raceName) || !alreadyAddedRaceSeason.contains(season)){
                    String CircuitToTurn = JSON.serialize(Race.get('Circuit'));
                    Map<String,Object> Circuit = (Map<String,Object>) JSON.deserializeUntyped(CircuitToTurn);
                    Race__c newRace = new Race__c(Name=raceName,
                                                 Season__c = season,
                                                 Race_URL__c = JSON.serialize(Race.get('url')).replaceAll('"',''),
                                                 Round__c = JSON.serialize(Race.get('round')).replaceAll('"',''),
                                                 Circuit__c = JSON.serialize(Circuit.get('circuitName')).replaceAll('"',''),
                                                 Circuit_URL__c = JSON.serialize(Circuit.get('url')).replaceAll('"',''),
                                                 Date__c = JSON.serialize(Race.get('date')).replaceAll('"',''),
                                                 Time__c = JSON.serialize(Race.get('time')).replaceAll('"',''));
                    addRaces.add(newRace);
                }
            }
            insert addRaces;
        }
    }
    public static List<Map<String,Object>> getCurrentConstructors(){
        Http http = new Http();
        HttpRequest request = new HttpRequest();
        request.setEndpoint('http://ergast.com/api/f1/'+year+'/constructors.JSON');//Fetches current constructors
        request.setMethod('GET');
        HttpResponse response = http.send(request);
        if(response.getStatusCode() == 200){
            //Parses the response as necessary
            Map<String,Object> respMap = (Map<String,Object>) JSON.deserializeUntyped(response.getBody());
            String MRData = JSON.serialize(respMap.get('MRData'));
            Map<String,Object> MRDataMap = (Map<String,Object>) JSON.deserializeUntyped(MRData);
            String ConstructorTable = JSON.serialize(MRDataMap.get('ConstructorTable'));
            Map<String,Object> ConstructorTableMap = (Map<String,Object>) JSON.deserializeUntyped(ConstructorTable);
            String Constructors = JSON.serialize(ConstructorTableMap.get('Constructors'));
            List<Object> ConstructorsObjects = (List<Object>) JSON.deserializeUntyped(Constructors);
            List<Map<String,Object>> ConstructorsMap = new List<Map<String,Object>>();
            for(Object ConstructorObj : ConstructorsObjects){
                Map<String,Object> hold = (Map<String,Object>) ConstructorObj;
                ConstructorsMap.add(hold);
            }
            return ConstructorsMap;
        }
        return null;
    }
    @future
    public static void createCurrentConstructors(){
        List<Map<String,Object>> Constructors = CalloutsClass.getCurrentConstructors();//Fetches current constructors
        if(Constructors!=null){//Checks success
            List<Constructor__c> addConstructors = new List<Constructor__c>();
            List<Constructor__c> alreadyAddedConstructors = [Select Name, Id FROM Constructor__c];//Queries already existing as to not duplicate
            List<String> alreadyAddedNames = new List<String>();
            for(Constructor__c hold : alreadyAddedConstructors){
                alreadyAddedNames.add(hold.Name);
            }
            for(Map<String,Object> Constructor : Constructors){//Creates constructor object if not already made.
                String constructorName = JSON.serialize(Constructor.get('name')).replaceAll('"','');
                if(!alreadyAddedNames.contains(constructorName)){
                    Constructor__c toAdd = new Constructor__c(Name=constructorName,
                                                             Nationality__c=JSON.serialize(Constructor.get('nationality')).replaceAll('"',''),
                                                             Constructor_URL__c=JSON.serialize(Constructor.get('url')).replaceAll('"',''));
                    addConstructors.add(toAdd);
                }
            }
            insert addConstructors;
        }
    }
    public static List<Map<String,Object>> getCurrentResults(){
        Boolean raceAvailable = True;//Use this variable to get around query limits from the API
        List<Map<String,Object>> raceResults = new List<Map<String,Object>>();
        Integer i = 1;
        While(raceAvailable){
            Http http = new Http();
        	HttpRequest request = new HttpRequest();
        	request.setEndpoint('http://ergast.com/api/f1/'+year+'/'+i+'/results.JSON');//Queries results of round i in the year
            i=i+1;//goes ahead and ups the i so that I don't forget to put that later
        	request.setMethod('GET');
        	HttpResponse response = http.send(request);
        	if(response.getStatusCode() == 200){
                //Parse through the result to what we want
            	Map<String,Object> respMap = (Map<String,Object>) JSON.deserializeUntyped(response.getBody());
            	String MRData = JSON.serialize(respMap.get('MRData'));
            	Map<String,Object> MRDataMap = (Map<String,Object>) JSON.deserializeUntyped(MRData);
            	String RaceTable = JSON.serialize(MRDataMap.get('RaceTable'));
            	Map<String,Object> RaceTableMap = (Map<String,Object>) JSON.deserializeUntyped(RaceTable);
            	String Races = JSON.serialize(RaceTableMap.get('Races'));
            	List<Object> RacesObjects = (List<Object>) JSON.deserializeUntyped(Races);
                if(RacesObjects.size()>0){//Checks the size of the race list, if it is not 1, then the round does not exist, so it changes to false and ends the loop
            		for(Object raceObj : RacesObjects){
                		Map<String,Object> hold = (Map<String,Object>) raceObj;
                		raceResults.add(hold);
            		}
                }
                else{
                    raceAvailable=False;
                }
        	}
            else{//If response failed, it cuts the loop
                raceAvailable=False;
            }
        }
        return raceResults;
    }
    @future
    public static void createRaceResults(){
        List<Map<String,Object>> raceResults = CalloutsClass.getCurrentResults();//Fetches the results, which iterates through all of the current year results
        List<Driver_Race_Placement__c> alreadyAddedResults = [SELECT Name FROM Driver_Race_Placement__c];//Creates a list of already existent results as to not create duplicates
        List<String> alreadyAddedResultNames = new List<String>();
        for(Driver_Race_Placement__c drp : alreadyAddedResults){
            alreadyAddedResultNames.add(drp.Name);
        }
        List<Race__c> Races = [SELECT Name, season__c, Id FROM Race__c];
        Map<String,Race__c> RaceMap = new Map<String,Race__c>();
        for(Race__c r : Races){
            RaceMap.put(r.Name, r);//Maps races based on name so they can be properly related in the results
        }
        List<Driver__c> Drivers = [SELECT Name, Id FROM Driver__c];
        Map<String,Driver__c> DriverMap = new Map<String,Driver__c>();
        for(Driver__c d : Drivers){
            DriverMap.put(d.Name, d);//Maps drivers to their name to be properly related
        }
        List<Constructor__c> Constructors = [SELECT Name, Id FROM Constructor__c];
        Map<String,Constructor__c> ConstructorMap = new Map<String,Constructor__c>();
        for(Constructor__c c : Constructors){
            ConstructorMap.put(c.Name, c);//Maps constructors to name for proper relationships to be formed
        }
        List<Driver_Race_Placement__c> toAdd = new List<Driver_Race_Placement__c>();
        for(Map<String,Object> raceResultMap : raceResults){//Parses through results map to get information we want
            String season = JSON.serialize(raceResultMap.get('season')).replaceAll('"','');
            String raceName = JSON.serialize(raceResultMap.get('raceName')).replaceAll('"','');
            String Results = JSON.serialize(raceResultMap.get('Results'));
            Race__c currentRace = RaceMap.get(raceName);//Fetches race based on current result
            List<Object> ResultsObjs = (List<Object>) JSON.deserializeUntyped(Results);
            List<Map<String,Object>> ResultsList = new List<Map<String,Object>>();
            for(Object robj : ResultsObjs){
                Map<String,Object> hold = (Map<String,Object>) robj;
                ResultsList.add(hold);
            }//Adds to results list of maps
            for(Map<String,Object> result : ResultsList){//Goes through and creates result while relating to drivers and constructors and placement, ensures no duplicate, parses through the rest of the results maps
                Map<String,Object> Driver = (Map<String,Object>) result.get('Driver');
                String DriverName = JSON.serialize(Driver.get('givenName')).replaceAll('"','') + ' ' + JSON.serialize(Driver.get('familyName')).replaceAll('"','');
                String placementName = DriverName+' Placement For '+raceName+' '+season;
                if(!alreadyAddedResultNames.contains(placementName)){
                    Map<String,Object> Constructor = (Map<String,Object>) result.get('Constructor');
                    String constructorName = JSON.serialize(Constructor.get('name')).replaceAll('"','');
                    Constructor__c currentConstructor = ConstructorMap.get(constructorName);
                    Driver__c currentDriver = DriverMap.get(DriverName);
                    String placement = JSON.serialize(result.get('position')).replaceAll('"','');
                    String points = JSON.serialize(result.get('points')).replaceAll('"','');
                    String grid = JSON.serialize(result.get('grid')).replaceAll('"','');
                    String laps = JSON.serialize(result.get('laps')).replaceAll('"','');
                    String status = JSON.serialize(result.get('status')).replaceAll('"','');
                    Driver_Race_Placement__c placementToAdd = new Driver_Race_Placement__c(Name=placementName,//Creates new placement object and populates it
                                                                                          Constructor__c=currentConstructor.Id,
                                                                                          Driver__c=currentDriver.Id,
                                                                                          Grid__c=grid,
                                                                                          Laps__c=laps,
                                                                                          Points__c=points,
                                                                                          Position__c=placement,
                                                                                          Race__c=currentRace.Id,
                                                                                          Season__c=season,
                                                                                          Status__c=status);
                    toAdd.add(placementToAdd);
                }
            }
        }
        insert toAdd;
    }
}