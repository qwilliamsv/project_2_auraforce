public class ListController {
    @AuraEnabled
    public static List<Driver__c> getDrivers(){
        return [SELECT name, nationality__c, Date_of_Birth__c, Permanent_Number__c, Id FROM Driver__c];
    }
    @AuraEnabled
    public static List<Driver_List__c> getLists(){
        Id userId = UserInfo.getUserId();//Auto-filter
        return [SELECT Name, Number_of_Drivers__c, Id FROM Driver_List__c WHERE ownerId =: userId];
    }
    @AuraEnabled
    public static List<Race_List__c> getRaceLists(){
        Id userId = UserInfo.getUserId();//Auto-filter
        return [SELECT Name, Number_of_Races__c, Id FROM Race_List__c WHERE ownerId =: userId];
    }
    @AuraEnabled
    public static List<Driver_List__c> retrieveLists(string toRetrieve){
        List<Object> holder2 = (List<Object>) system.JSON.deserializeUntyped(toRetrieve);//Used to retrieve specified data requested from component
        List<String> Ids = new List<String>();
        for(Object heldagain : holder2){//Iterates through the list and parses the JSON so that it works
            String ided = JSON.serialize(heldagain);
            Map<String,Object> testMap = (Map<String,Object>)JSON.deserializeUntyped(ided);
            Object finalId = testMap.get('Id');
            String finalFinal = (String)JSON.serialize(finalId);//Multiple attempts were had here, hence why there are three finals in the working one
            String finalFinalFinal = finalFinal.replaceAll('"', '');
            Ids.add(finalFinalFinal);
        }
        List<Driver_List__c> toSend = [SELECT Name, Number_of_Drivers__c, Id FROM Driver_List__c WHERE Id IN :Ids];
        return toSend;
    }
    @AuraEnabled
    public static void makeList(string name){
        Driver_List__c toAdd = new Driver_List__c(Name=name);
        insert toAdd;
    }
    @AuraEnabled
    public static void makeRaceList(string name){
        Race_List__c toAdd = new Race_List__c(Name=name);
        insert toAdd;
    }
    @AuraEnabled
    public static string removeDriverLists(String toRemove){
        List<Object> holder2 = (List<Object>) system.JSON.deserializeUntyped(toRemove);//For the delete button, again not pretty because it parses through the sent JSON
        List<String> Ids = new List<String>();
        for(Object heldagain : holder2){
            String ided = JSON.serialize(heldagain);
            Map<String,Object> testMap = (Map<String,Object>)JSON.deserializeUntyped(ided);
            Object finalId = testMap.get('Name');
            String finalFinal = (String)JSON.serialize(finalId);
            String finalFinalFinal = finalFinal.replace('"', '');
            Ids.add(finalFinalFinal);
        }
        List<Driver_List__c> toDelete = [SELECT Id, Name FROM Driver_List__c WHERE Name in :Ids];
        delete toDelete;
        return 'removed';
    }
    @AuraEnabled
    public static string removeRaceLists(String toRemove){
        List<Object> holder2 = (List<Object>) system.JSON.deserializeUntyped(toRemove);//Similar to the removeDriverLists, but for Race Lists
        List<String> Ids = new List<String>();
        for(Object heldagain : holder2){
            String ided = JSON.serialize(heldagain);
            Map<String,Object> testMap = (Map<String,Object>)JSON.deserializeUntyped(ided);
            Object finalId = testMap.get('Name');
            String finalFinal = (String)JSON.serialize(finalId);
            String finalFinalFinal = finalFinal.replace('"', '');
            Ids.add(finalFinalFinal);
        }
        List<Race_List__c> toDelete = [SELECT Id, Name FROM Race_List__c WHERE Name in :Ids];
        delete toDelete;
        return 'removed';
    }
    @AuraEnabled
    public static void addToListApex(String driversToAdd, String listsToAddTo){
        system.debug('Running');//Apex code of the addToList button
        system.debug(listsToAddTo);
        List<Object> holder2 = (List<Object>) system.JSON.deserializeUntyped(driversToAdd);
        system.debug(holder2);
        List<String> Ids = new List<String>();
        for(Object heldagain : holder2){
            String ided = JSON.serialize(heldagain);
            Map<String,Object> testMap = (Map<String,Object>)JSON.deserializeUntyped(ided);
            Object finalId = testMap.get('Id');
            String finalFinal = (String)JSON.serialize(finalId);
            String finalFinalFinal = finalFinal.replaceAll('"', '');
            Ids.add(finalFinalFinal);
        }//For loop fetches all required Ids of Drivers
        List<Object> holder3 = (List<Object>) system.JSON.deserializeUntyped(listsToAddTo);
        List<String> listIds = new List<String>();
        for(Object heldagain : holder3){
            String ided = JSON.serialize(heldagain);
            Map<String,Object> testMap = (Map<String,Object>)JSON.deserializeUntyped(ided);
            Object finalId = testMap.get('Id');
            String finalFinal = (String)JSON.serialize(finalId);
            String finalFinalFinal = finalFinal.replaceAll('"', '');
            listIds.add(finalFinalFinal);
        }//Fetches Ids of Lists to add too
        system.debug(Ids);
        system.debug(listIds);
        List<Driver_To_List__c> toAdd = new List<Driver_To_List__c>();
        List<Driver__c> toAddToLists = [SELECT Name, Id FROM Driver__c WHERE Id IN :Ids];
        List<Driver_List__c> listsForAdding = [SELECT Name, Id FROM Driver_List__c WHERE Id IN :listIds];
        for(Driver_List__c listItem : listsForAdding){
            for(Driver__c driver : toAddToLists){//Creates the junction object that adds drivers to lists
                Driver_To_List__c addThis = new Driver_To_List__c(Name=driver.Name+' to '+listItem.Name,
                                                                 Driver_List__c=listItem.Id,
                                                                 Driver__c=driver.Id);
                toAdd.add(addThis);
            }
        }
        insert toAdd;
    }
}