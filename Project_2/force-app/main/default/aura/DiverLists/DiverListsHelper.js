({
	init : function(component) {
		component.set('v.columns',[
            {label: 'Name', fieldName: 'linkName', type: 'url', typeAttributes:{label:{fieldName: 'Name'},target:'_blank'}},
            {label: '# of Drivers', fieldName: 'Number_of_Drivers__c', type: 'text'}
        ]);
        let getLists = component.get("c.getLists");
        getLists.setCallback(this, function(response){
            if(response.getState() === "SUCCESS"){
                var records =response.getReturnValue();
                records.forEach(function(record){
                    record.linkName='https://f-1-developer-edition.na139.force.com/s/detail/'+record.Id;
                });
                component.set("v.Lists",records);
            }
        });
        $A.enqueueAction(getLists)
	},
    sendRows : function(component) {
        let listOfLists = component.find('Table').getSelectedRows();
        let ListInfo = JSON.stringify(listOfLists);
        let sendInfo = $A.get("e.c:ListSender");
        sendInfo.setParams({
            "ListInfo":ListInfo
        });
        sendInfo.fire();
    },
    createList : function(component) {
        let listName = component.get("v.listName");
        let makeList = component.get("c.makeList");
        makeList.setParams({name : listName});
        makeList.setCallback(this, function(response){
            if(response.getState() === "SUCCESS"){
                component.set("v.listName", "List Created");
            }
        });
        $A.enqueueAction(makeList);
        let getLists = component.get("c.getLists");//Make-shift force update, not pretty to look at here, but it causes almost immediate refresh in the component.
        getLists.setCallback(this, function(response){
            if(response.getState() === "SUCCESS"){
                var records =response.getReturnValue();
                records.forEach(function(record){
                    record.linkName='https://f-1-developer-edition.na139.force.com/s/detail/'+record.Id;
                });
                component.set("v.Lists",records);
            }
        });
        $A.enqueueAction(getLists)
    },
    deleter : function(component, event, helper) {
        let listOfLists = component.find('Table').getSelectedRows();
        let listHold = JSON.stringify(listOfLists);
        let deleteLists = component.get("c.removeDriverLists");
        deleteLists.setParams({toRemove : listHold});
        deleteLists.setCallback(this, function(response){
        	if(response.getState() === "SUCCESS"){
            	component.set("v.listName", "Lists Deleted");
        	}
       });
       $A.enqueueAction(deleteLists);
       let getLists = component.get("c.getLists");//Again, makeshift force update, not pretty code-wise, but works every time I've tested it in component
       getLists.setCallback(this, function(response){
           if(response.getState() === "SUCCESS"){
               var records =response.getReturnValue();
               records.forEach(function(record){
                   record.linkName='https://f-1-developer-edition.na139.force.com/s/detail/'+record.Id;
               });
               component.set("v.Lists",records);
           }
       });
       $A.enqueueAction(getLists)
    }

})