({
	init : function(component) {
		component.set('v.columns',[
            {label: 'Permanent Number', fieldName: 'Permanent_Number__c', type: 'text'},
            {label: 'Name', fieldName: 'linkName', type: 'url', typeAttributes:{label:{fieldName: 'Name'},target:'_blank'}},
            {label: 'Nationality', fieldName: 'Nationality__c', type: 'text'},
            {label: 'Date of Birth', fieldName: 'Date_of_Birth__c', type: 'text'}
        ]);
        let getDrivers = component.get("c.getDrivers");
        getDrivers.setCallback(this, function(response){
            if(response.getState() === "SUCCESS"){
                var records =response.getReturnValue();
                records.forEach(function(record){
                    record.linkName='https://f-1-developer-edition.na139.force.com/s/detail/'+record.Id;
                });
                component.set("v.drivers",records);
            }
        });
        $A.enqueueAction(getDrivers);
	},
    sendRows : function(component) {
        let listOfDrivers = component.find('Table').getSelectedRows();
        let driverInfo = JSON.stringify(listOfDrivers);
        let sendInfo = $A.get("e.c:DiverInfoSent");
        sendInfo.setParams({
            "DriverInfo":driverInfo
        });
        sendInfo.fire();
    },
    listRetrieval : function(component, event){
        console.log("Retrieving");
        let lists = event.getParam("ListInfo");
        let sendList = component.get("c.retrieveLists");
        sendList.setParams({toRetrieve : lists});
        sendList.setCallback(this, function(response){
            if(response.getState() === "SUCCESS"){
                console.log("Retrieved");
                component.set("v.Lists", response.getReturnValue());
                console.log(component.get("v.Lists"));
            }
            else{
                console.log("Failed to Retrieve");
            }
        });
        $A.enqueueAction(sendList);
    },
    addToList : function(component) {
        console.log("Run");
        let Lists = component.get("v.Lists");
        let listsToSend = JSON.stringify(Lists);
        let listOfDrivers = component.find('Table').getSelectedRows();
        let driverInfo = JSON.stringify(listOfDrivers);
        console.log(driverInfo);
        console.log(listsToSend);
        let addDriversToLists = component.get("c.addToListApex");
        addDriversToLists.setParams({driversToAdd : driverInfo, listsToAddTo : listsToSend});
        addDriversToLists.setCallback(this, function(response){
            if(response.getState() === "SUCCESS"){
                component.set("v.displaySuccess","True");
            }
            else {
                console.log("Failed to Add");
                console.log(response.getState())
            }
        });
        $A.enqueueAction(addDriversToLists);
        let refreshEvent = $A.get("e.c:Listrefresh");
        refreshEvent.fire();
    },
    closeMessage : function(component) {
        console.log("Trying to close");
        component.set("v.displaySuccess",!component.get("v.displaySuccess"));
    }
})