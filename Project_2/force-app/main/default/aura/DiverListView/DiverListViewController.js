({
	init : function(component, event, helper) {
		helper.init(component);
	},
    sendSelectedRows : function(component,event,helper){
        helper.sendRows(component);
    },
    addToList : function(component,event,helper){
        helper.addToList(component);
    },
    listRetrieval : function(component,event,helper){
        helper.listRetrieval(component,event);
    },
    closeMessage: function(component,event,helper){
        helper.closeMessage(component);
    }
})