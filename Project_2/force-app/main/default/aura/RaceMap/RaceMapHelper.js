({
	doinit : function(component) {
		let raceList = component.get("c.getCourseLocations");
        raceList.setCallback(this, function(response){
            if(response.getState() === "SUCCESS"){
                let markers = [];
                for(let currentMap of response.getReturnValue()){
                    markers.push({
                        location:{
                            Latitude: currentMap.lat.replaceAll('"',''),
                            Longitude: currentMap.long.replaceAll('"','')
                        },
                        title: currentMap.raceName.replaceAll('"',''),
                        description: 'Held at the '+currentMap.circuitName.replaceAll('"','')+' on '+currentMap.date.replaceAll('"','')+' at '+currentMap.time.replaceAll('"','')
                    });
                }
                component.set("v.mapMarkers",markers);
            }
        });
        $A.enqueueAction(raceList);
	}
})